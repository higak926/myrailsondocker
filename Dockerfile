FROM ruby:2.5.8
RUN apt-get update -qq && apt-get install -y nodejs default-mysql-client git
RUN mkdir /myRails3
WORKDIR /myRails3
COPY Gemfile /myRails3/Gemfile
COPY Gemfile.lock /myRails3/Gemfile.lock
RUN bundle install
COPY . /myRails3

# sshキーの追加
ADD .ssh /root/.ssh
RUN chmod 600 /root/.ssh/*

RUN git clone git@github.com:higak926/$myAppOnDocker

# Add a script to be executed every time the container starts.
COPY entrypoint.sh /usr/bin/
RUN chmod +x /usr/bin/entrypoint.sh
ENTRYPOINT ["entrypoint.sh"]
EXPOSE 3000

# Start the main process.
CMD ["rails", "server", "-b", "0.0.0.0"]
